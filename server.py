from flask import Flask, request, Response
from flask_restful import Resource, Api
from flask_cors import CORS
from controllers.InformeConsolidado import InformeConsolidado
import json
import datetime

app = Flask(__name__)
CORS(app)
api = Api(app)

def myconverter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()

class Informe(Resource):
    def get(self):
        response = InformeConsolidado.list()
        return response

    def post(self):
        data = request.get_json(force=True)
        lastInsertedId = InformeConsolidado.save(data)
        return Response({"id": lastInsertedId}, status=201, mimetype='application/json')

    def put(self):
        data = request.get_json(force=True)
        result = InformeConsolidado.update(data)
        if(result):
            return data
        else:
            return Response("", status=404, mimetype='application/json')

class InformeIndividual(Resource):

    def get(self, id):
        result = InformeConsolidado.getById(id)
        if(result != None):
            return Response(json.dumps(result, default = myconverter), status=404, mimetype='application/json')
        else:
            return Response("", status=404, mimetype='application/json')

    def delete(self, id):
        result = InformeConsolidado.deleteById(id)
        if(result):
            return Response("", status=200, mimetype='application/json')
        else:
            return Response("", status=404, mimetype='application/json')

api.add_resource(Informe, '/informe')
api.add_resource(InformeIndividual, '/informe/<string:id>')