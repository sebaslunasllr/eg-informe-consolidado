from flask import jsonify, request
import db.db as db
import datetime

class InformeConsolidado():
    def save(data):
        try:
            conn = db.open_connection()
            cur = conn.cursor()
            query = "INSERT INTO informe_consolidados(file_type, date_from, date_to, send_to, info_name, url) VALUES(%s, %s, %s, %s, %s, %s)"
            cur.execute(query, (data['file_type'],data['date_from'], data['date_to'], data['send_to'], data['info_name'], data["url"]))
            conn.commit()
            return cur.lastrowid
        except Exception as ex:
            print("Could not insert data ", data)
            print(ex)
            return ""
        finally:
            conn.close()

    def list():
        conn = db.open_connection()
        cur = conn.cursor()
        json_list = []
        query = "SELECT * FROM informe_consolidados"
        cur.execute(query)
        rows = cur.fetchall()
        for row in rows: 
            json_list.append(InformeConsolidado.buildEntity(row))
        conn.close()
        return jsonify(json_list)
    
    def getById(id):
        try:
            conn = db.open_connection()
            cur = conn.cursor()
            query = "SELECT * FROM informe_consolidados WHERE id=?"
            cur.execute(query, (id,))
            result = cur.fetchone()
            entity = InformeConsolidado.buildEntity(result)
            return entity
        except Exception as ex:
            print(ex)
            return False
        finally: 
            conn.close()
        
    def deleteById(id):
        try:
            conn = db.open_connection()
            cur = conn.cursor()
            print("delete")
            if(InformeConsolidado.getById(id) != None):
                query = "DELETE FROM informe_consolidados WHERE id=?"
                cur.execute(query,(id,))
            else:
                print("id not found")
                return False
            
        except Exception as ex:
            print(ex)
            return False    
        finally:
            conn.close()
            return True

    def update(data):
        try:
            conn = db.open_connection()
            cur = conn.cursor()
            if(InformeConsolidado.getById(data['id']) != None):
                query = "UPDATE informe_consolidados SET info_name=? WHERE id=?"
                cur.execute(query, (data['info_name'], data['id']))
                conn.close()
                return True
            else:
                print("id not found")
                return False
        except Exception as ex:
            print("Could not insert data ", data)
            print(ex)
            return False
        finally:
            conn.close()
            return True

    def buildEntity(data):
        id, created_date, file_type, date_from, date_to, send_to, info_name, url = data
        return {
            "id": id,
            "created_date": InformeConsolidado.myconverter(created_date),
            "file_type":file_type,
            "date_from": InformeConsolidado.myconverter(date_from),
            "date_to": InformeConsolidado.myconverter(date_to),
            "send_to": send_to,
            "info_name":info_name,
            "url": url
        }

    def myconverter(o):
        if isinstance(o, datetime.datetime):
            return o.__str__()