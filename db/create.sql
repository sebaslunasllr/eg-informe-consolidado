create table informe_consolidados
(
    id int(10) auto_increment,
    creation_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    file_type VARCHAR(50),
    date_from TIMESTAMP,
    date_to TIMESTAMP,
    send_to VARCHAR(50),
    info_name VARCHAR(50) not null,
    url VARCHAR(50) not null,
    constraint informe_consolidados_pk
        primary key (id)
);